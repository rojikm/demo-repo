provider "aws" {
  region = "us-west-1"
}


resource "aws_vpc" "main" {
  count = 1
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main vpc 1122 Jenkins Check"
  }
}


output "vpc_id_out"{
  value = aws_vpc.main
}



